<div class="container-image {{ join(' ', $additional_classes) }}" style="padding-bottom: {{ 100 / $ratio }}%">
    @if (is_int($image))
        {!! wp_get_attachment_image($image, $size, false, ['class' => 'ratio-image']) !!}
    @else
        <img class="ratio-image" src="{{ $image['url'] }}" srcset="{{ wp_get_attachment_image_srcset($image['ID'], $size) }}" alt="{{ $image['alt'] }}">
    @endif
</div>
