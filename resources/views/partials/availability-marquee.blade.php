<p class="marquee marquee--inline inline relative pt-1 pb-1 mb-0 border-2 border-white rounded-3xl text-sm overflow-hidden">
    <span class="opacity-0 inline-block height-0 pl-2 pr-2">{!! $availability_text !!}<span class="text-primary ml-2 mr-2">//</span></span>
    <span class="marquee-banner pl-2 pr-2 whitespace-no-wrap leading-none">
        @for ($i = 0; $i <= 2; $i++)
            <span class="marquee-text">{!! $availability_text !!} <span class="text-primary ml-2 mr-2">//</span></span>
        @endfor
    </span>
</p>
