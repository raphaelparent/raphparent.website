<header class="pl-6 pr-6 flex items-center justify-between">
    <a class="brand pt-8 pb-8 lg:flex-1 uppercase text-lg lg:text-sm font-semibold  text-white" href="{{ home_url('/') }}">{{ get_bloginfo('name', 'display') }}</a>
    <div class="hidden ml-auto md:flex lg:ml-0 lg:flex-1 justify-center">
        @include('partials/availability-marquee')
    </div>
    <nav class="nav-primary flex flex-col flex-1 justify-end overflow-hidden lg:flex-row lg:items-end" id="menu-container">
        @if (has_nav_menu('primary_navigation'))
            {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
        <div class="nav-footer mt-6 lg:hidden">
            <div class="px-6 flex justify-end mb-6 md:hidden">
                @include('partials/availability-marquee')
            </div>
            {{-- <div class="hidden xs:block"> --}}
            @include('partials/footer-credit')
            {{-- </div> --}}
        </div>
    </nav>
    <button class="burger-button uppercase font-semibold p-6 -mr-6 lg:hidden" id="nav-toggler" data-menu-label="{{ __('Menu.', 'raphparent') }}" data-close-label="{{ __('Close.', 'raphparent') }}">
        {{ _e('Menu.', 'raphparent') }}
    </button>
</header>
