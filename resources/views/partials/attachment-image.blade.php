@if (is_int($image))
    {!! wp_get_attachment_image($image, $size, false, ['class' => join(' ', $additional_classes)]) !!}
@else
    <img src="{{ $image['url'] }}" class="{{ join(' ', $additional_classes) }}" srcset="{{ wp_get_attachment_image_srcset($image['ID'], $size) }}" alt="{{ $image['alt'] }}">
@endif
