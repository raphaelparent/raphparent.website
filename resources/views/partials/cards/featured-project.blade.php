<article class="featured-project cursor-hitbox relative border-t border-white border-opacity-25 last:border-b">
    <a href="{{ $project['link'] }}" target="_blank" class="block text-white hover:text-white pb-8 pt-24 lg:pb-24 lg:pt-64">
        <h2 class="featured-project-title relative z-20 leading-none lg:text-7xl">{{ $project['title'] }}</h2>
        <p class="relative z-20 mt-6 mb-8 lg:mb-0">
            <span class="uppercase block lg:inline ">{{ $project['for_whom'] }}</span>
            <span class="text-primary mr-3 lg:mx-3 ">//</span>
            <span class="opacity-50">{{ $project['role'] }}</span>
        </p>
        <div class="featured-project-image cursor z-10 w-full lg:fixed lg:w-2/4">
            @include('partials.attachment-image', [
                'image' => $project['thumbnail'],
                'size' => 'featured-project-card',
                'additional_classes' => ['w-full'],
                'default_alt' => $project['title'],
            ])
        </div>
    </a>
</article>
