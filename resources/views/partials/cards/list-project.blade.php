@php
$wrapper_class = 'flex w-full max-w-full py-8 px-6 items-center text-white hover:text-black';
$disabled = 'false';
@endphp

@if (!$project['link'])
    @php
        $wrapper_class .= ' pointer-events-none';
        $disabled = 'true';
    @endphp
@endif

<article class="list-project table-hover-row last:border-b border-t border-white border-opacity-25" data-table-hover-disabled="{{ $disabled }}">
    <a href="{{ $project['link'] }}" target="_blank" class="{{ $wrapper_class }}">
        <h3 class="uppercase mb-0 pr-4 flex-grow-0 flex-shrink-0 w-3/6 md:w-2/6 font-semibold text-lg tracking-widest md:text-xl md:pl-12 xl:w-5/12">{{ $project['title'] }}</h3>
        <p class="mb-0 pr-4 flex-grow-0 flex-shrink-0 hidden w-2/6 sm:block xl:w-3/12">{{ $project['role'] }}</p>
        <p class="mb-0 pr-4 flex-grow-0 flex-shrink-0 w-3/6 text-right sm:w-1/6 md:text-left">{{ $project['year'] }}</p>
        <p class="mb-0 flex-grow-0 flex-shrink-0 hidden mt-4 md:block md:mt-0 md:w-1/6 md:pr-12">
            @if ($project['link'])
                <span class="uppercase">{{ __('Voir le projet', 'raphparent') }}</span>
            @else
                <span class="opacity-50">{{ __('Hors ligne', 'raphparent') }}</span>
            @endif
        </p>
    </a>
</article>
