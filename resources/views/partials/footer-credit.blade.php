<div class="flex flex-col px-6 py-8 border-t border-white border-opacity-25 sm:flex-row sm:justify-between">
    <p class="uppercase mb-0 text-right">{{ sprintf(__('Raphaël Parent © %s', 'raphparent'), date('Y')) }}</p>
    <p class="uppercase mt-4 text-right sm:mt-0">{!! sprintf(__('Conception visuel par %s', 'raphparent'), '<a href="' . $designer['url'] . '" class="text-white">' . $designer['title'] . '</a>') !!}</p>
</div>
