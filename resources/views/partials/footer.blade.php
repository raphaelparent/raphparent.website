<footer class="overflow-hidden">
    <div class="marquee relative flex flex-1 justify-center border-t border-white border-opacity-25" id="{{ __('contact', 'raphparent') }}">
        <a href="mailto:{{ $contact_email }}" title="{{ $contact_email }}" class="marquee-banner w-full flex items-center uppercase py-20 text-4xl md:text-6xl text-white hover:text-white">
            @for ($i = 0; $i <= 3; $i++)
                <span class="marquee-text opacity-1 pl-12 flex items-center">
                    {!! $contact_email !!}
                    <span class="inline ml-12">
                        <svg width="26" height="24" viewBox="0 0 26 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M17.5858 6.56805L10.7929 13.7988L15.1834 24L20.2959 12L25.4201 0L12.7101 2.95858L0 5.91716L9.28994 12.0118L17.5858 6.56805Z" fill="#82ED4F" />
                        </svg>
                    </span>
                </span>
            @endfor
        </a>
    </div>
    @include('partials/footer-credit')
</footer>
