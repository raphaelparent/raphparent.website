@extends('layouts.app')

@section('content')
    <section class="hero relative min-h-hero flex items-center sm:justify-center">
        <h1 class="leading-none font-secondary text-6xl xs:text-7xl sm:text-center md:text-8xl md:mb-16">
            {!! $heading !!}
        </h1>
        @if (!wp_is_mobile())
            <button id="scroll-cta" class="scroll-cta hidden marquee-vertical absolute h-12 w-8 left-0 bottom-0 mb-8 text-center overflow-hidden border-2 border-white border-opacity-25 rounded-3xl md:block">
                <span class="marquee-vertical-inner absolute block">
                    @for ($i = 0; $i < 8; $i++)
                        <svg xmlns="http://www.w3.org/2000/svg" class="marquee-vertical-inner-text h-6 w-6 inline-block" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 17l-4 4m0 0l-4-4m4 4V3" />
                        </svg>
                    @endfor
                </span>
            </button>
        @endif
    </section>
    <section class="offset-title overflow-hidden mb-24 md:mt-40 md:mb-64 flex flex-col justify-center md:items-center" id="{{ __('projets', 'raphparent') }}">
        <div class="flex flex-col md:w-full md:items-start lg:flex-row lg:justify-around lg:items-end lg:max-w-5xl">
            <h2 class="animated-title ml-0 font-secondary text-center leading-none md:ml-20 md:text-left md:text-7xl lg:ml-0">{!! $selected_works['title'] !!}</h2>
            <p class="max-w-xs mt-8 mx-auto md:mx-0 md:ml-auto md:mt-0 md:relative lg:ml-0">{!! $selected_works['text'] !!}</p>
        </div>
    </section>
    <section class="mb-48 md:mb-48 md:px-12 xl:px-32">
        @foreach ($featured_projects as $project)
            @include('partials/cards/featured-project', ['project' => $project])
        @endforeach
    </section>
    <section class="mt-48 md:mt-64 mb-56">
        <div class="offset-title flex justify-center md:justify-start md:px-32 xl:px-56">
            <h2 class="font-secondary block text-center md:text-7xl">{{ _e('<span class="full">Mentions</span> <span class="outline">honorables</span>', 'raphparent') }}</h2>
        </div>
        <div class="-mx-6 mt-16">
            <div class="flex w-full max-w-full pb-4 px-6">
                <p class="uppercase pr-4 text-sm opacity-50 flex-shrink-0 w-3/6 md:w-2/6 md:pl-12 xl:w-5/12">{{ _e('Projet', 'raphparent') }}</p>
                <p class="hidden uppercase pr-4 text-sm opacity-50 flex-shrink-0 w-2/6 sm:block xl:w-3/12">{{ _e('Rôle', 'raphparent') }}</p>
                <p class="uppercase pr-4 text-sm opacity-50 flex-shrink-0 w-3/6 text-right sm:w-1/6 md:text-left">{{ _e('Année', 'raphparent') }}</p>
                <p class="hidden uppercase text-sm opacity-50 flex-shrink-0 w-1/6 pr-12 md:block">{{ _e('Lien', 'raphparent') }}</p>
            </div>
            <div class="table-hover">
                @foreach ($additional_projects as $project)
                    @include('partials/cards/list-project', ['project' => $project])
                @endforeach
            </div>
        </div>
    </section>
@endsection
