@extends('layouts.app')

@section('content')
    <div style="min-height: calc(100vh - 88px - 263px);" class="flex items-center justify-center">
        <h1 class="text-center">
            <p class="mb-0 text-sm uppercase leading-none">{{ __('Erreur 404', 'raphparent') }}</p>
            <p class="font-secondary text-6xl">{{ __('Y\'a rien icitte!', 'raphparent') }}</p>
        </h1>
    </div>
@endsection
