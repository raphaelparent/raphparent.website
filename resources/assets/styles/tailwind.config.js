/*
|-------------------------------------------------------------------------------
| Tailwind – The Utility-First CSS Framework
|-------------------------------------------------------------------------------
|
| Documentation at https://tailwindcss.com
|
*/

/**
 * Global Styles Plugin
 *
 * This plugin modifies Tailwind’s base styles using values from the theme.
 * https://tailwindcss.com/docs/adding-base-styles#using-a-plugin
 */
const globalStyles = ({ addBase, config }) => {
    addBase({
        a: {
            color: config('theme.textColor.primary'),
            textDecoration: 'none',
            borderBottom: '1px solid transparent',
            transition: '0.2s ease'
        },
        'a:hover': {
            color: config('theme.colors.primary')
        },
        p: {
            marginBottom: config('theme.margin.3'),
            lineHeight: config('theme.lineHeight.normal')
        },
        'h1, h2, h3, h4, h5': {
            marginBottom: config('theme.margin.2'),
            lineHeight: config('theme.lineHeight.tight')
        },
        h1: { fontSize: config('theme.fontSize.6xl') },
        h2: { fontSize: config('theme.fontSize.5xl') },
        h3: { fontSize: config('theme.fontSize.4xl') },
        h4: { fontSize: config('theme.fontSize.3xl') },
        h5: { fontSize: config('theme.fontSize.2xl') },
        'ol, ul': { paddingLeft: config('theme.padding.4') },
        ol: { listStyleType: 'decimal' },
        ul: { listStyleType: 'disc' }
    });
};

/**
 * Configuration
 */
module.exports = {
    theme: {
        colors: {
            primary: '#82ED4F',
            black: '#121111',
            white: '#fff',
            gray: {
                100: '#f7fafc',
                200: '#edf2f7',
                300: '#e2e8f0',
                400: '#cbd5e0',
                500: '#a0aec0',
                600: '#718096',
                700: '#4a5568',
                800: '#2d3748',
                900: '#1a202c'
            },
            transparent: 'transparent'
        },
        shadows: {
            outline: '0 0 0 3px rgba(82,93,220,0.3)'
        },
        backgroundImage: {
            noise: "url('../images/noise.png')"
        },
        fontFamily: {
            body: ['"Clash Display"', 'Helvetica', 'Arial', 'sans-serif'],
            secondary: ['"Pirata One"', 'serif']
        },
        screens: {
            xs: '480px',
            sm: '640px',
            md: '768px',
            lg: '1024px',
            xl: '1280px',
            xxl: '1440px',
            xxxl: '1600px'
        },
        extend: {
            fontSize: {
                '7xl': '5rem',
                '8xl': '7rem'
            },
            minHeight: {
                hero: 'calc(100vh - var(--header-h))'
            }
        }
    },
    variants: {
        borderWidth: ['responsive', 'last', 'hover', 'focus']
    },
    plugins: [
        globalStyles
    ],
    corePlugins: {
        container: false
    }
};
