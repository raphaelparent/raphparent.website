/**
 * rpCustomCursor
 *
 * TODO:
 * - Allow for props override when initiating;
 */
export default function () {
    const props = {
        cursorSelector: '.cursor',
        hitboxSelector: '.cursor-hitbox',
        withVelocity: true
    };

    const state = {
        cursors: []
    };

    /**
     * moveCursor
     *
     * Modify the cursor position to be centered with the mouse.
     * Animate with velocity if gsap is present and withVelocity is enbaled.
     */
    const moveCursors = (e) => {
        const mouseY = e.clientY;
        const mouseX = e.clientX;

        if (gsap && props.withVelocity) {
            gsap.to(props.cursorSelector, {
                duration: 1,
                ease: 'power3.out',
                y: function (index, target) {
                    return mouseY - target.offsetHeight / 2;
                },
                x: function (index, target) {
                    return mouseX - target.offsetWidth / 2;
                }
            });
        } else {
            for (let i = 0; i < state.cursors.length; i++) {
                const cursor = state.cursors[i];
                cursor.elem.style.top = (mouseY - cursor.elem.offsetHeight / 2) + 'px';
                cursor.elem.style.left = (mouseX - cursor.elem.offsetWidth / 2) + 'px';
            }
        }
    };

    /**
     * showCursor
     *
     * Add a visible class to the cursor and hide the default cursor
     */
    const showCursor = (e) => {
        const cursor = e.currentTarget.querySelector(props.cursorSelector);
        cursor.classList.add('cursor-visible');
    };

    /**
     * hideCursor
     *
     * Remove the visible class from the cursor and reset default cursor
     */
    const hideCursor = (e) => {
        const cursor = e.currentTarget.querySelector(props.cursorSelector);
        cursor.classList.remove('cursor-visible');
    };

    /**
     * initSingleCursor
     */
    const initSingleCursor = (cursor) => {
        const computedCursorTransform = window.getComputedStyle(cursor.elem).getPropertyValue('transform');

        if (computedCursorTransform && computedCursorTransform !== 'none') {
            cursor.existingTransform = computedCursorTransform;
        }

        // Events listener
        cursor.hitbox.addEventListener('mouseenter', showCursor);
        cursor.hitbox.addEventListener('mouseleave', hideCursor);

        const newLength = state.cursors.push(cursor);
        cursor.elem.dataset.index = newLength - 1;
    };

    /**
     * initCursors
     */
    const initCursors = () => {
        const cursors = document.querySelectorAll(props.cursorSelector);

        for (let i = 0; i < cursors.length; i++) {
            const cursor = cursors[i];
            const hitbox = cursor.closest(props.hitboxSelector);

            if (hitbox) {
                const cursorData = {
                    elem: cursor,
                    hitbox: hitbox
                };

                initSingleCursor(cursorData);
            }
        }

        if (state.cursors.length > 0) {
            window.addEventListener('mousemove', moveCursors);
        }
    };

    if (window.innerWidth > 1024) {
        initCursors();
    }
}
