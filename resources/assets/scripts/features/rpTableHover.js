/**
 * rpTableHover
 */
export default function () {
    const props = {
        tableSelector: '.table-hover',
        rowSelector: '.table-hover-row'
    };

    const state = {
        table: null,
        highlight: null,
        lastEnteredDirection: ''
    };

    /**
     * onTableMouseEnter
     *
     *  @param Object e The mouseenter event object.
     *
     * Animate in the higlight element.
     */
    const onTableMouseEnter = (e) => {
        const direction = getMouseEnterDirection(e);
        const rows = state.table.querySelectorAll(props.rowSelector);
        const gsapStyles = {
            y: 0
        };

        if (direction === 'top') {
            const firstRow = state.table.querySelector(`${props.rowSelector}:first-of-type`);
            state.highlight.style.transformOrigin = 'top center';
            gsapStyles.bottom = 'auto';
            gsapStyles.top = '0';
            gsapStyles.height = firstRow.offsetHeight + 'px';
        } else {
            const lastRow = state.table.querySelector(`${props.rowSelector}:last-of-type`);
            state.highlight.style.transformOrigin = 'bottom center';
            gsapStyles.top = 'auto';
            gsapStyles.bottom = '0';
            gsapStyles.height = lastRow.offsetHeight + 'px';
        }

        gsap.set(state.highlight, gsapStyles);
        gsap.to(state.highlight, {
            duration: 0.24,
            scaleY: 1,
            ease: 'power1.out'
        });

        for (let i = 0; i < rows.length; i++) {
            rows[i].addEventListener('mouseenter', onRowMouseEnter);
        }

        state.lastEnteredDirection = direction;
    };

    /**
     * onTableMouseLeave
     *
     *  @param Object e The mouseleave event object.
     *
     * Animate out the higlight element.
     */
    const onTableMouseLeave = (e) => {
        const direction = getMouseLeaveDirection(e);
        const rows = state.table.querySelectorAll(props.rowSelector);

        if (direction === 'top') {
            state.highlight.style.transformOrigin = 'top center';
        } else {
            state.highlight.style.transformOrigin = 'bottom center';
        }

        gsap.to(state.highlight, {
            duration: 0.24,
            scaleY: 0,
            ease: 'power1.out'
        });

        for (let i = 0; i < rows.length; i++) {
            rows[i].removeEventListener('mouseenter', onRowMouseEnter);
        }
    };

    /**
     * onRowMouseEnter
     *
     *  @param Object e The mouseenter event object.
     *
     * Move the highlight element based on the hovered rows. Adds a class when the row is disabled.
     */
    const onRowMouseEnter = (e) => {
        const row = e.currentTarget;
        const rowRelativeRect = getRelativeToParentBoundingClientRect(row, state.table);
        let y = rowRelativeRect.top;

        if (state.lastEnteredDirection === 'bottom') {
            y = rowRelativeRect.top - (state.table.offsetHeight - row.offsetHeight);
        }

        if (row.dataset.tableHoverDisabled === 'true') {
            state.highlight.classList.add(props.tableSelector.substring(1) + '-highlight--disabled');
        } else {
            state.highlight.classList.remove(props.tableSelector.substring(1) + '-highlight--disabled');
        }

        gsap.to(state.highlight, {
            duration: 0.24,
            y: y,
            height: row.offsetHeight,
            ease: 'power1.out'
        });
    };

    /**
     * getMouseEnterDirection
     *
     * @param Object e The mouseenter event object.
     *
     * @return String Either 'top' or 'bottom' depending on the result.
     */
    const getMouseEnterDirection = (e) => {
        const elemRect = e.currentTarget.getBoundingClientRect();
        const elemMiddlePos = Math.round(elemRect.y + elemRect.height / 2);

        if (e.clientY <= elemMiddlePos) {
            return 'top';
        } else {
            return 'bottom';
        }
    };

    /**
     * getMouseLeaveDirection
     *
     * @param Object e The mouseleave event object.
     *
     * @return String Either 'top' or 'bottom' depending on the result.
     */
    const getMouseLeaveDirection = (e) => {
        const elemRect = e.currentTarget.getBoundingClientRect();

        if (e.clientY < elemRect.y) {
            return 'top';
        } else {
            return 'bottom';
        }
    };

    /**
     * getRelativeToParentBoundingClientRect
     *
     * @param Node child The child element to get the position for.
     * @param Node aprent The parent element to calculate the child position with.
     *
     * @return Object An object with the top, right, bottom and left of the child element relative to the parent.
     */
    const getRelativeToParentBoundingClientRect = (child, parent) => {
        const childRect = child.getBoundingClientRect();
        const parentRect = parent.getBoundingClientRect();

        return {
            top: childRect.top - parentRect.top,
            right: childRect.right - parentRect.right,
            bottom: childRect.bottom - parentRect.bottom,
            left: childRect.left - parentRect.left
        };
    };

    /**
     * setupHightlightElem
     *
     * Add the higlight element to the table.
     */
    const setupHightlightElem = () => {
        const highlightElementClass = props.tableSelector.substring(1) + '-highlight';
        state.table.insertAdjacentHTML('afterbegin', `<span class="${highlightElementClass}"></span>`);
        state.highlight = state.table.querySelector(`.${highlightElementClass}`);
    };

    /**
     * initTableHover
     *
     * Set the props, state, DOM and event listeners.
     */
    const initTableHover = () => {
        // Get the table
        state.table = document.querySelector(props.tableSelector);

        // DOM Manipulation
        setupHightlightElem();

        // Event handlers
        state.table.addEventListener('mouseenter', onTableMouseEnter);
        state.table.addEventListener('mouseleave', onTableMouseLeave);
    };

    /**
     * Initialize the module.
     */
    if (window.innerWidth > 640) {
        initTableHover();
    }
}
