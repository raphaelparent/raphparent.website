export default function (marquee, duration) {
    const props = {
        marquee: marquee,
        duration: duration
    };

    /**
     * Setup the marquee elements
     */
    function setupTheMarquee (marquee) {
        const texts = marquee.querySelectorAll('.marquee-text');

        if (texts.length > 0) {
            marquee.setAttribute('style', '--marquee-width:' + '-' + texts[0].offsetWidth + 'px;' + '--marquee-height:' + texts[0].offsetHeight + 'px;');

            window.setTimeout(() => {
                gsap.set(texts, {
                    x: (i) => i * texts[i].offsetWidth
                });

                gsap.to(texts, {
                    duration: props.duration || 10,
                    ease: 'none',
                    x: '-=' + texts[0].offsetWidth,
                    repeat: -1
                });
            }, 100); // wait for the font to be loaded, otherwise x is not set properly
        }
    }

    setupTheMarquee(props.marquee);
}
