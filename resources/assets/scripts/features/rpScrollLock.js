export default function (lock) {
    const lockScroll = () => {
        const scrollPosition = window.pageYOffset;

        document.body.classList.add('overflow-hidden');
        window.localStorage.setItem('scrollPosition', scrollPosition);
        window.scrollTo({ top: 0, behavior: 'auto' });
    };

    const unlockScroll = () => {
        const scrollPosition = window.localStorage.getItem('scrollPosition');

        document.body.classList.remove('overflow-hidden');
        window.scrollTo({ top: scrollPosition, behavior: 'auto' });
    };

    if (lock) {
        lockScroll();
    } else {
        unlockScroll();
    }
}
