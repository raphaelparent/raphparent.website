import rpMarquee from '../features/rpMarquee';
import rpScrollLock from '../features/rpScrollLock';

export default {
    init() {
        /**
         * Toggle the mobile navigation
         */
        const initNavToggler = () => {
            const navToggler = document.getElementById('nav-toggler');

            if (navToggler) {
                navToggler.addEventListener('click', (e) => {
                    e.currentTarget.classList.toggle('show-close');
                    document.getElementById('menu-container').classList.toggle('nav-primary-show');

                    const isOpen = e.currentTarget.classList.contains('show-close');
                    rpScrollLock(isOpen);

                    if (isOpen) {
                        e.currentTarget.innerHTML = e.currentTarget.dataset.closeLabel;
                    } else {
                        e.currentTarget.innerHTML = e.currentTarget.dataset.menuLabel;
                    }
                });
            }
        };

        initNavToggler();

        /**
         * Scroll to next section
         */
        const initScrollCta = () => {
            const scrollCta = document.getElementById('scroll-cta');

            if (scrollCta) {
                scrollCta.addEventListener('click', (e) => {
                    const section = e.currentTarget.closest('section').nextElementSibling;
                    const scrollToY = section.getBoundingClientRect().top + window.pageYOffset - 200;
                    window.scrollTo({ top: scrollToY, behavior: 'smooth' });
                });
            }
        };

        /**
         * Smooth scroll on anchor element
         */
        const anchors = document.querySelectorAll('a[href*="#"]');

        const smoothScrollToAnchor = function (e) {
            const id = e.currentTarget.href.split('#')[1];
            const element = document.getElementById(id);

            if (!element) {
                return;
            }

            e.preventDefault();
            const scrollOffset = -100;
            const scrollTo = element.getBoundingClientRect().top + window.pageYOffset + scrollOffset;

            rpScrollLock(false);
            document.getElementById('menu-container').classList.remove('nav-primary-show');
            window.scrollTo({ top: scrollTo, behavior: 'smooth' });
        };

        for (let i = 0; i < anchors.length; i++) {
            anchors[i].addEventListener('click', smoothScrollToAnchor);
        }

        /**
         * Hide or show the scroll cta element
         */
        const toggleScrollCta = () => {
            const scrollCta = document.getElementById('scroll-cta');

            if (window.scrollY > 20) {
                scrollCta.classList.add('scroll-cta-hidden');
            } else {
                scrollCta.classList.remove('scroll-cta-hidden');
            }
        };

        initScrollCta();
        window.addEventListener('scroll', toggleScrollCta);

        /**
         * Calculate header height
         */
        const calcHeaderHeight = () => {
            const header = document.querySelector('header');
            document.body.style.setProperty('--header-h', header.offsetHeight + 'px');
        };

        calcHeaderHeight();
        window.addEventListener('resize', calcHeaderHeight);

        /**
         * Header marquee
         */
        const headerMarquee = document.querySelector('header .marquee');
        rpMarquee(headerMarquee, 6);

        /**
         * Mobile navigation marquee
         */
        const navMarquee = document.querySelector('.nav-footer .marquee');
        rpMarquee(navMarquee, 6);

        /**
         * Footer marquee
         */
        const footerMarquee = document.querySelector('footer .marquee');
        rpMarquee(footerMarquee, 12);
    },
    finalize() {
        // JavaScript to be fired on all pages, after page specific JS is fired
    }
};
