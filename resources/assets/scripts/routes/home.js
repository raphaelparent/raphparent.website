import rpCustomCursor from '../features/rpCustomCursor';
import rpTableHover from '../features/rpTableHover';

export default {
    // JavaScript to be fired on the home page
    init () {
        gsap.registerPlugin(ScrollTrigger);

        rpCustomCursor();
        rpTableHover();

        /**
         * Animated title animation
         */
        const titles = document.querySelectorAll('.animated-title');

        for (let i = 0; i < titles.length; i++) {
            const full = titles[i].querySelector('.full');
            const outline = titles[i].querySelector('.outline');

            full.style.display = 'block';
            outline.style.display = 'block';

            gsap.timeline({
                scrollTrigger: {
                    elem: full,
                    trigger: titles[i],
                    scrub: 1,
                    end: 'bottom top'
                }
            }).from(full, {
                x: -100
            });

            gsap.timeline({
                scrollTrigger: {
                    elem: outline,
                    trigger: titles[i],
                    scrub: 1,
                    end: 'bottom top'
                }
            }).from(outline, {
                x: 100,
                ease: 'power1.out'
            });
        }
    },
    // JavaScript to be fired on the home page, after the init JS
    finalize () {
    }
};
