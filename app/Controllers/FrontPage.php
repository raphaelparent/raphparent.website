<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public function heading()
    {
        return get_field('title');
    }

    public function selected_works()
    {
        return get_field('selected_works');
    }

    public function featured_projects()
    {
        $featured_projects_ids = get_field('featured_projects');
        $featured_projects = [];

        foreach ($featured_projects_ids as $id)
        {
            $featured_projects[] = [
                'title' => get_the_title($id),
                'for_whom' => get_field('for_whom', $id),
                'role' => get_field('role', $id),
                'thumbnail' => get_post_thumbnail_id($id),
                'link' => get_field('external_link', $id)
            ];
        }

        return $featured_projects;
    }

    public function additional_projects()
    {
        $featured_projects_ids = get_field('featured_projects');
        $projects = [];
        $projects_ids = get_posts([
            'post_type' => 'projects',
            'numberposts' => -1,
            'exclude' =>  $featured_projects_ids,
            'orderby' => 'date',
            'order' => 'DESC',
            'fields' => 'ids'
        ]);

        foreach ($projects_ids as $id)
        {
            $projects[] = [
                'title' => get_the_title($id),
                'role' => get_field('role', $id),
                'year' => get_the_date('Y', $id),
                'link' => get_field('external_link', $id)
            ];
        }

        return $projects;
    }
}
