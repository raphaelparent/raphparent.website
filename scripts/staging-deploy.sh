#!/bin/bash

# Steps to make this work properly
# 1. Create a new web user on the droplet and assign it to the www-data group
#    https://www.digitalocean.com/community/questions/setting-up-a-new-user-using-ssh
# 2. Make sure composer is properly installed
# 3. Update the user and ip address for the ssh connection

DEPLOY_USER="root"
DEPLOY_HOST="143.198.44.150"
GIT_BRANCH="master"
PROJECT_FOLDER="/var/www/html/wp-content/themes/raphparent-website"

echo -e "\n⬆️  Deploying current repository files\n"
echo -e "🌲 $GIT_BRANCH"
echo -e "👤 $DEPLOY_USER"
echo -e "🖥  $DEPLOY_HOST\n"

# SSH and execute these commands
ssh $DEPLOY_USER@$DEPLOY_HOST << EOF
    cd $PROJECT_FOLDER;
    git checkout $GIT_BRANCH;
    git pull;
    npm run build;
EOF

echo -e "\n✅  File deployment done"
echo -e "\n❗  Check above output for possible error\n"
